use crate::common;

use std::io::Write;

/// Top-level handler that DEFLATE compresses and responds with from a &[u8] body
/// If None passed to status, 200 OK will be returned
pub async fn bytes_handler(
    body: &[u8],
    content_type: &str,
    status: Option<hyper::StatusCode>,
) -> common::Result<hyper::Response<hyper::Body>> {
    // Compress
    let mut e = flate2::write::ZlibEncoder::new(Vec::new(), flate2::Compression::default());
    e.write_all(body).unwrap();
    let compressed = e.finish().unwrap();
    // Return response
    Ok(hyper::Response::builder()
        .status(status.unwrap_or_default())
        .header(hyper::header::CONTENT_TYPE, content_type)
        .header(hyper::header::CONTENT_ENCODING, "deflate")
        .body(hyper::Body::from(compressed))
        .unwrap())
}

/// Pass string through to bytes_handler
pub async fn string_handler(
    body: &str,
    content_type: &str,
    status: Option<hyper::StatusCode>,
) -> common::Result<hyper::Response<hyper::Body>> {
    bytes_handler(body.as_bytes(), content_type, status).await
}

pub async fn html_str_handler(body: &str) -> common::Result<hyper::Response<hyper::Body>> {
    string_handler(body, "text/html", None).await
}
