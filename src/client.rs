use crate::common;
use crate::relay;

use hyper::body::Buf;

#[derive(Clone, Debug)]
pub struct Hub {
    client: hyper::client::Client<hyper::client::HttpConnector>,
    url: hyper::Uri,
}

#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub struct Relay {
    state: String,
    kind: String,
}

impl Hub {
    /// Returns a new Hub
    ///
    /// # Arguments
    ///
    /// * `url` - Connection string to an InfluxDB
    /// * `relay` - a City tag referenced by InfluxDB
    ///
    /// # Example
    ///
    /// ```
    /// use gengi::Hub;
    ///
    /// let hub = Hub::new("http://localhost:5000");
    /// ```
    pub fn new(url: &str) -> Hub {
        Hub {
            client: hyper::Client::new(),
            url: format!("{}/api", url).parse().unwrap(),
        }
    }

    async fn post_command(&self, command: relay::Command) -> common::Result<Relay> {
        let req = hyper::Request::builder()
            .method("POST")
            .uri(&self.url)
            .header("content-type", "application/json")
            .body(hyper::Body::from(serde_json::to_string(&relay::Action {
                command: command,
            })?))
            .unwrap();

        let res = self.client.request(req).await?;

        // asynchronously aggregate the chunks of the body
        let body = hyper::body::aggregate(res).await?;

        // try to parse as json with serde_json
        let relay = serde_json::from_reader(body.reader())?;

        Ok(relay)
    }

    /// Switch relay
    ///
    /// # Example
    ///
    /// ```
    /// use gengi::Hub;
    ///
    /// let hub = Hub::new("http://localhost:5000");
    ///
    /// let relay = hub.switch();
    /// ```
    pub async fn switch(&self) -> common::Result<Relay> {
        self.post_command(relay::Command::Switch).await
    }

    /// Pulse relay
    ///
    /// # Arguments
    ///
    /// * `dur_ms` - duration of pulse (in milliseconds)
    ///
    /// # Example
    ///
    /// ```
    /// use gengi::Hub;
    ///
    /// let hub = Hub::new("http://localhost:5000");
    ///
    /// let relay = hub.pulse(50);
    /// ```
    pub async fn pulse(&self, dur_ms: u64) -> common::Result<Relay> {
        self.post_command(relay::Command::Pulse(dur_ms)).await
    }

    /// Set ON relay
    ///
    /// # Example
    ///
    /// ```
    /// use gengi::Hub;
    ///
    /// let hub = Hub::new("http://localhost:5000");
    ///
    /// let relay = hub.on();
    /// ```
    pub async fn on(&self) -> common::Result<Relay> {
        self.post_command(relay::Command::On).await
    }

    /// Set OFF relay
    ///
    /// # Example
    ///
    /// ```
    /// use gengi::Hub;
    ///
    /// let hub = Hub::new("http://localhost:5000");
    ///
    /// let relay = hub.off();
    /// ```
    pub async fn off(&self) -> common::Result<Relay> {
        self.post_command(relay::Command::Off).await
    }

    /// Get relay
    ///
    /// # Example
    ///
    /// ```
    /// use gengi::Hub;
    ///
    /// let hub = Hub::new("http://localhost:5000");
    ///
    /// let relay = hub.get();
    /// ```
    pub async fn get(&self) -> common::Result<Relay> {
        let req = hyper::Request::builder()
            .method("GET")
            .uri(&self.url)
            .body(hyper::Body::empty())
            .unwrap();

        let res = self.client.request(req).await?;

        // asynchronously aggregate the chunks of the body
        let body = hyper::body::aggregate(res).await?;

        // try to parse as json with serde_json
        let relay = serde_json::from_reader(body.reader())?;

        Ok(relay)
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_hub() {
        let hub = Hub::new("http://localhost:12345");

        assert!(hub.off().await.is_ok());
        assert!(hub.get().await.unwrap().state == "Off");

        assert!(hub.on().await.is_ok());
        assert!(hub.get().await.unwrap().state == "On");

        assert!(hub.switch().await.is_ok());
        assert!(hub.get().await.unwrap().state == "Off");

        assert!(hub.pulse(50).await.is_ok());
        assert!(hub.get().await.unwrap().state == "Off");

        assert!(hub.on().await.is_ok());
        assert!(hub.pulse(50).await.is_ok());
        assert!(hub.get().await.unwrap().state == "Off");
    }
}
